package com.example.practica0292_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    // Declaración de componentes del layout
    private lateinit var txtAltura: EditText;
    private lateinit var txtPeso: EditText;
    private lateinit var lblIMC: TextView;
    private lateinit var btnCalcular: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnCerrar: Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relacionar componentes del layout
        txtAltura = findViewById(R.id.txtAltura) as EditText;
        txtPeso = findViewById(R.id.txtPeso) as EditText;
        lblIMC = findViewById(R.id.lblIMC) as TextView;
        btnCalcular = findViewById(R.id.btnCalcular) as Button;
        btnLimpiar = findViewById(R.id.btnLimpiar) as Button;
        btnCerrar = findViewById(R.id.btnCerrar) as Button;


        // Metodo clic del Button: btnCalcular
        btnCalcular.setOnClickListener {

            var altura: Float;
            var peso: Float;
            var IMC: Float;

            if(!txtAltura.text.toString().contentEquals("") &&
                !txtPeso.text.toString().contentEquals("")){

                altura = txtAltura.text.toString().toFloat();
                peso = txtPeso.text.toString().toFloat();
                IMC = peso / (altura*altura);

                lblIMC.setText(((IMC*100.0).roundToInt()/100.0).toString());

            }
            else{
                Toast.makeText(applicationContext,
                    "Se necesitan capturar tanto el peso como la altura.",
                    Toast.LENGTH_SHORT).show();
            }
        }


        // Metodo clic del Button: btnLimpiar
        btnLimpiar.setOnClickListener {
            txtAltura.setText("");
            txtPeso.setText("");
            lblIMC.setText("");
        }


        // Metodo clic del Button: btnCerrar
        btnCerrar.setOnClickListener {
            finish();
        }
    }
}